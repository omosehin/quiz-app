let quest = questionsSet;


const output = [];
function quizapp() {
    const quizContainer = document.getElementById("review");
    
    let chosenAnswers = localStorage.getItem('choosenAnswers');
    let arr = chosenAnswers.split(",");
    newarr = [];
    arr.map((item, i) =>newarr.push(item));

    questionsSet.forEach((currentQuestion, index) => {
        const answers = [];

        for (letter in currentQuestion.answers) {
            answers.push(
                `<label class ="label">
                 <input type="radio" name="question${index}" value="${letter}" class = 'reviewInput' >
                  ${currentQuestion.answers[letter]}  

            </label>`
            );
        }
        output.push(
            `<div class="slide">
               <div class="question"> ${currentQuestion.question} </div>
               <div class="answers reviewInput"> ${answers.join("")} </div>
               <p class = "Correctanswer"> Correct answer is ${currentQuestion.correctAnswer}</p>
             </div>`
        );
    });

    quizContainer.innerHTML = output.join("");
}
quizapp();

