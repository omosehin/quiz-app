var questionsSet = [
    
    {
        question: "The poor quality of selection will mean extra cost on _______ and supervision.?",
        answers: {
            a: "Training",
            b: "Recruitment",
            c: 'Work quality'
        },
        correctAnswer: 'b'
    },
    {
        question: "Which one is correct team name in NBA?",
        answers: {
            a: "New York Bulls",
            b: "Los Angeles Kings",
            c: "Huston Rocket"
        },
        correctAnswer: "a"
    },

    {
        question: "Who is the Line Manager Of PowerTech?",
        answers: {
            a: "Abayomi",
            b: "Philip",
            c: "Sinmi"
        },
        correctAnswer: "c"
    },
    {
        question: "Which of the following act deals with recruitment and selection?",
        answers: {
            a: ' Child labour act',
            b: 'The apprentices act',
            c: 'Mines act'
        },
        correctAnswer: "b"
    },
    {
        question: "Who is the strongest?",
        answers: {
          a: "Superman",
          b: "The Terminator",
          c: "Waluigi, obviously"
        },
        correctAnswer: "c"
      }
];