let myBar = document.getElementById("myBar");
let timer = false;
let width = 20;

console.log("initial width", width);
let startQuiz = true;
let showQuiz = true;
var j = 0;
function countDown(secs, elem) {
  var element = document.getElementById(elem);
  element.innerHTML = `Test Holds for ${secs} seconds`;
  if (secs < 1) {
    clearTimeout(timer);
    element.innerHTML = `
    <h2>Timed Out</h2>
    `;
    showResults();
  }
  secs--;
  var timer = setTimeout("countDown (" + secs + ',"' + elem + '")', 1000);
}

function quizapp() {
  // we'll need a place to store the HTML output
  const output = [];
  // for each question...
  questionsSet.forEach((currentQuestion, index) => {
    // we'll want to store the list of answer choices
    const answers = [];

    // and for each available answer...
    for (letter in currentQuestion.answers) {
      // ...add an HTML radio button
      answers.push(
        `<label>
               <input type="radio" name="question${index}" value="${letter}">
                ${currentQuestion.answers[letter]}    
             </label>`
      );
    }

    // add this question and its answers to the output
    output.push(
      `<div class="slide">
             <div class="question"> ${currentQuestion.question} </div>
             <div class="answers"> ${answers.join("")} </div>
           </div>`
    );
  });

  // finally combine our output list into one string of HTML and put it on the page
  quizContainer.innerHTML = output.join("");
}

function showResults() {
  let choosen = [];
  // gather answer containers from our quiz
  const answerContainers = quizContainer.querySelectorAll(".answers");

  // keep track of user's answers
  let numCorrect = 0;

  // for each question...
  questionsSet.forEach((currentQuestion, index) => {
    // find selected answer
    const answerContainer = answerContainers[index];
    const selector = `input[name=question${index}]:checked`;

    const userAnswer = (answerContainer.querySelector(selector) || {}).value;

    if (userAnswer === currentQuestion.correctAnswer) {
      choosen.push(userAnswer);
      numCorrect++;
      answerContainers[index].style.color = "lightgreen";
    } else {
      choosen.push(userAnswer);

      answerContainers[index].style.color = "red";
    }
    document.getElementById("timeOut").style.display = "none";
  });

  localStorage.setItem("choosenAnswers", choosen);
  resultsContainer.innerHTML = `${numCorrect} out of ${questionsSet.length} questions were correct`;
  document.querySelector(".reviewTag").style.display = "block";
  document.querySelector("button.startBtn").textContent = "Take the Quiz";
  clearTimeout(Timer);
}

function showSlide(n) {
  slides[currentSlide].classList.remove("active-slide");
  slides[n].classList.add("active-slide");
  currentSlide = n;

  if (currentSlide === 0) {
    previousButton.style.display = "none";
  } else {
    previousButton.style.display = "inline-block";
  }

  if (currentSlide === slides.length - 1) {
    nextButton.style.display = "none";
    submitButton.style.display = "inline-block";
    j = 1;
  } else {
    nextButton.style.display = "inline-block";
    submitButton.style.display = "none";
  }
}

function showNextSlide() {
  showSlide(currentSlide + 1);
  myBar.style.width = width + 20 + "%";
  myBar.innerHTML = width + 20 + "%";
  width += 20;
}

function showPreviousSlide() {
  showSlide(currentSlide - 1);
  let newWidth = width < 20 ? 20 : width;
  myBar.style.width = newWidth - 20 + "%";
  myBar.innerHTML = newWidth - 20 + "%";
  width -= 20;
}

const quizContainer = document.getElementById("quiz");
const resultsContainer = document.getElementById("results");
const submitButton = document.getElementById("submit");

quizapp();

const previousButton = document.getElementById("previous");
const nextButton = document.getElementById("next");
const slides = document.querySelectorAll(".slide");
let currentSlide = 0;

showSlide(0);

// on submit, show results
submitButton.addEventListener("click", showResults);
previousButton.addEventListener("click", showPreviousSlide);
nextButton.addEventListener("click", showNextSlide);

document.querySelector(".startBtn").addEventListener("click", e => {
  e.preventDefault();
  if (j === 1) {
    window.location.reload();
  }
  if (
    document.querySelector("button.startBtn").textContent == "Take the Quiz" &&
    startQuiz
  ) {
    document.querySelector(".quizStrength").style.display = "flex";
    document.querySelector(".quixMainContainer").style.background = "black";
    document.querySelector(".reviewTag").style.display = "none";
    document.querySelector(".joblogo").style.display = "none";
  } else {
    var element = document.getElementById("timeOut");
    var results = document.getElementById("results");
    document.querySelector(".quizStrength").style.display = "none";
    element.style.display = "none";
    results.style.display = "none";
    leaveStatement.style.display = "block";
    YesNo.style.display = "block";
    nextButton.style.display = "none";
    previousButton.style.display = "none";
    document.querySelector(".reviewTag").style.display = "none";
    document.querySelector(".joblogo").style.display = "none";
  }
});
var leaveStatement = document.getElementById("leaveStatement");
var YesNo = document.getElementById("YesNo");
let easy = document.querySelector(".easy1");
easy.addEventListener("click", e => {
  e.preventDefault();
  countDown(60, (get = "timeOut"));
  document.querySelector(".QuizContainer").style.display = "block";
  document.querySelector(".quizStrength").style.display = "none";
  document.querySelector("button.startBtn").textContent = "Stop Test";
  leaveStatement.style.display = "none";
  YesNo.style.display = "none";
  document.querySelector(".reviewTag").style.display = "none";
  // document.querySelector('.quizStrength').style.display = 'none';
});

document.querySelector(".Yes").addEventListener("click", () => {
  window.location.reload();
});
document.querySelector(".No").addEventListener("click", () => {
  var element = document.getElementById("timeOut");
  element.style.display = "block";
  nextButton.style.display = "block";
  previousButton.style.display = "block";
  results.style.display = "block";
  leaveStatement.style.display = "none";
  YesNo.style.display = "none";
  document.querySelector(".reviewTag").style.display = "none";
});
